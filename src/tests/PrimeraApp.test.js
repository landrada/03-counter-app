import React from 'react';
import '@testing-library/jest-dom'
import { render } from '@testing-library/react';
import PrimeraApp from '../PrimeraApp';
import { shallow } from 'enzyme';

describe('Pruebas en <PrimeraApp/>', () => {
    // test('debe mostrar el mensaje "Hola soy Pacita"', () => {

    //     const saludo ="Hola Soy Pacita";

    //     const { getByText } = render(<PrimeraApp saludo={ saludo } />);

    //     expect (getByText(saludo)).toBeInTheDocument();
    // })

    test('debe de mostrar <PrimeraApp/> correctamente', () => {

        const saludo = "Hola Soy Lucas";

        const wrapper = shallow(<PrimeraApp saludo={saludo} />);

        expect(wrapper).toMatchSnapshot();
    })


    test('debe de mostrar si subtitulo enviado por props', () => {

        const saludo = "Hola Soy Lucas";
        const subTitulo = "Soy un subtitulo";

        const wrapper = shallow(
            <PrimeraApp
                saludo={saludo}
                subTitulo={subTitulo}
            />);

        const textoParrafo = wrapper.find('p').text();
        expect(textoParrafo).toBe(subTitulo);

    })

})
