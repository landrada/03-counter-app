describe('Pruebas en el archivo demo.test.js', ()=> {
    
    test('Debe ser iguales los string', () => {
    
        //1. Inicialización
        const mensaje = 'Hola mundo';
    
        //2. Estimulo
        const mensaje2 = `Hola mundo`;
    
        expect(mensaje).toBe(mensaje2);
    
    })

});

