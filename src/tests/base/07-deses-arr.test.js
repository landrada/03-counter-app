import '@testing-library/jest-dom';
import { retornaArreglo } from '../../base/07-deses-arr';


describe('Pruebas en 07-deses-arr.test.js', ()=> {
    
    test('El metodo retornaArreglo debe retornar un string y un numero', () => {
    
        //1. Inicialización
        const [letras, numeros] = retornaArreglo();
        
        //2. Estimulo
        //


        expect(letras).toBe('ABC');
        expect(typeof letras).toBe('string');


        expect(numeros).toBe(123);
        expect(typeof numeros).toBe('number');
        
    })

});

