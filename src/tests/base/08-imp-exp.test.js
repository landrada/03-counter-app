import '@testing-library/jest-dom';
import { getHeroeById, getHeroesByOwner } from '../../base/08-imp-exp';
import heroes from '../../data/heroes';



describe('Pruebas en 08-imp-exp.test.js', ()=> {
    
    test('Debe retornar un héroe por id', () => {
    
        const id = 1;
        const heroe = getHeroeById(id);
        
        const heroeData = heroes.find(h => h.id ===id);

        expect(heroe).toEqual(heroeData);
        
    });

    test('Debe retornar un undefined si héroe no existe', () => {
    
        const id = 10;
        const heroe = getHeroeById(id);
        
    
        expect(heroe).toEqual(undefined);
        
    });


    test('Debe retornar un array con los héros de DC', () => {
    
        const owner = 'DC';
        const heroesTest = getHeroesByOwner(owner);
        
        const heroesData = heroes.filter( (heroe) => heroe.owner === owner );
    
        expect(heroesTest).toEqual(heroesData);
        
    });


    test('Debe retornar un array con los héros de Marvel', () => {
    
        const owner = 'Marvel';
        const heroesTest = getHeroesByOwner(owner);
        
        expect(heroesTest.length).toBe(2);
        
    })



});

