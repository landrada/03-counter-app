import '@testing-library/jest-dom';
import { getUser, getUsuarioActivo } from '../../base/05-funciones';

describe('Pruebas en 05-funciones.test.js', ()=> {
    
    test('El metodo getUser debe retornar un objeto', () => {
    
        //1. Inicialización
        const userTest = {
            uid: 'ABC123',
            username: 'El_Papi1502'
        }
    
        //2. Estimulo
        const user = getUser();
    
        expect(userTest).toEqual(user);
    
    })

    test('El metodo getUsuarioActivo debe retornar un objeto', () => {
    
        //1. Inicialización
        const name = 'Paz';
        const userActiveTest = {
            uid: 'ABC567',
            username: name,
        }
    
        //2. Estimulo
        const userActive = getUsuarioActivo(name);
    
        expect(userActive).toEqual(userActiveTest);
    
    })



});

