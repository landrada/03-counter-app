import '@testing-library/jest-dom'
import { getSaludo } from '../../base/02-template-string';

describe('Pruebas en 02-template-string.test.js', ()=> {
    
    test('El metodo getSaludo debe retornar Hola Lucas', () => {
    
        //1. Inicialización
        const name = 'Lucas';
    
        //2. Estimulo
        const saludo = getSaludo(name);
    
        expect(saludo).toBe('Hola '+ name);
    
    })

    test('El metodo getSaludo debe retornar Hola Paz si no hay argumento', () => {
    
        //1. Inicialización
        const name = 'Paz';
    
        //2. Estimulo
        const saludo = getSaludo();
    
        expect(saludo).toBe('Hola '+ name);
    
    })



});

