import '@testing-library/jest-dom';
import { getImagen } from '../../base/11-async-await';


describe('Pruebas en 11-async-await.test.js', ()=> {
    
    test('getImagen debe retornar la url de la imagen', async() => {
    
        const url = await getImagen();

        expect(typeof url).toBe('string');
                
    });

    // test('getImagen debe retornar un error al tratar de recuperar la url imagen', async() => {
    
    //     const url = await getImagen();

    //     expect(url.includes('https://')).toBe(true);
                
    // });

   

});

