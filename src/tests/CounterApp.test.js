import React from 'react';
import '@testing-library/jest-dom'
import { render } from '@testing-library/react';
import PrimeraApp from '../PrimeraApp';
import { shallow } from 'enzyme';
import CounterApp from '../CounterApp';

describe('Pruebas en <CounterApp/>', () => {

    let wrapper = shallow(<CounterApp />);

    beforeEach(()=> {
        wrapper = shallow(<CounterApp />);
    });

    test('debe de mostrar <CounterApp/> correctamente', () => {

        expect(wrapper).toMatchSnapshot();
    })



    test('debe de mostrar el valor por defecto de 100', () => {


        const wrapper = shallow(
            <CounterApp
            value={ 100 }
            />);

        const counterTexto = wrapper.find('h2').text().trim();
        expect(counterTexto).toBe('100');

    })


    test('debe incremetar con el boton +1', () => {

        wrapper.find('button').at(0).simulate('click');
        const counterTexto = wrapper.find('h2').text().trim();
        expect(counterTexto).toBe('11');

    })

    test('debe decrementar con el boton -1', () => {

        wrapper.find('button').at(2).simulate('click');
        const counterTexto = wrapper.find('h2').text().trim();
        expect(counterTexto).toBe('9');

    })

    test('debe inicilaizar con el boton Reset', () => {

        const wrapper = shallow(
            <CounterApp
            value={ 105 }
            />);
        wrapper.find('button').at(0).simulate('click');
        wrapper.find('button').at(0).simulate('click');
        wrapper.find('button').at(1).simulate('click');
        const counterTexto = wrapper.find('h2').text().trim();
        expect(counterTexto).toBe('105');

    })

})
