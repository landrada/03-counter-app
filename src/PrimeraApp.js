import React from 'react';
import PropTypes from 'prop-types';
// import React, { Fragment } from 'react';

const PrimeraApp = ({ saludo, subTitulo }) => {

    return (
        <>
            <h1> { saludo } </h1>
            {/* <pre> {  JSON.stringify( saludo, null, 3 )  } </pre> */}
            <p>{ subTitulo }</p>
        </>
    );
        
} 

PrimeraApp.propTypes = {
    saludo: PropTypes.string.isRequired
}

PrimeraApp.defaultProps = {
    subTitulo: 'Soy un subtitulo'
}


export default PrimeraApp;